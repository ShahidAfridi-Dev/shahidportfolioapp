export const personalData = {
  name: "Shahid Afridi",
  profile: '/shahid.jpeg',
  designation: "Software Developer",
  description: "My name is Shahid Afridi. I am a professional and enthusiastic programmer in my daily life. I am a quick learner with a self-learning attitude. I love to learn and explore new technologies and am passionate about problem-solving. I love almost all the stacks of web application development and love to make the web more open to the world. My core skill is based on JavaScript and I love to do most of the things using JavaScript.",
  email: 'shahidafridics@gmail.com',
  phone: '+91 9092745806',
  address: '1/71 Vilanthagandam Main Road Cholapuram - Kumbakonam - Tanjore - TamilNadu - India - 612503 ',
  github: 'https://github.com/Shahid-Afridii',
  facebook: 'https://www.facebook.com/profile.php?id=100013747351550',
  linkedIn: 'https://www.linkedin.com/in/shahid-afridi-648045203/',
  twitter: 'https://twitter.com/shahidafridics',
  devUsername: "Shahiddev",
  
}