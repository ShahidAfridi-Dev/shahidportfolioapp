export const educations = [
  {
    id: 1,
    title: "Bachelor Degree",
    duration: "2025 - 2018",
    institution: "Annai College of Arts & Science",
  },
  {
    id: 2,
    title: "Higher Secondary Certificate",
    duration: "2013 - 2014",
    institution: "Vallalar Higher Secondary School - Kumbakonam",
  },
  {
    id: 3,
    title: "Secondary School Certificate",
    duration: "2002 - 2012",
    institution: "Vallalar Higher Secondary School - Kumbakonam",
  }
]