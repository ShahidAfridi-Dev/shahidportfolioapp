import ayla from '/public/image/ayla.jpg';
import crefin from '/public/image/crefin.jpg';
import realEstate from '/public/image/real-estate.jpg';
import travel from '/public/image/travel.jpg';

export const projectsData = [
    {
        id: 1,
        name: 'JPencil v3: Next-Generation E-Commerce Platform',
        description: "Transform your digital storefront with JPencil v3, a dynamic e-commerce solution designed to compete with Shopify. This platform offers detailed customization from block-by-block homepage adjustments to sophisticated theme color choices and extensive font options. Empower your brand with the flexibility to design a unique shopping experience tailored to your audience. With JPencil v3, you can seamlessly integrate advanced e-commerce features, ensuring both functionality and style in a user-friendly environment.",
        tools: ['Next Js','Tailwind css', 'Node Js','Express', 'MySql', , 'Nginx'],
        role: 'Team Lead and FrontEnd Developer',
        code: '',
        demo: '',
        image: crefin,
    },
    {
        id: 2,
        name: 'JPencil v3 Dashboard:  E-Commerce Customization',
        description: 'Dive into the heart of  e-commerce operations with the JPencil v3 Dashboard, a central hub designed to streamline the creation and management of your online store. Effortlessly customize your homepage layout, choose from a diverse palette of themes, and handle domain management with ease. This dashboard enables you to bring your digital storefront to life, providing all the tools necessary to launch and maintain a dynamic e-commerce site. Seamlessly integrate functionality and aesthetics, ensuring your business thrives in the competitive online marketplace',
        tools: ['React Js', 'Tailwind CSS',  "NestJS", "TypeScript", "MySQL", "Sun-Editor"],
        role: 'Team Lead and FrontEnd Developer',
        code: '',
        demo: '',
        image: travel,
    },
    {
        id: 3,
        name: 'Quotation Generator :  Invoices and Tasks ',
        description: 'My team built an AI-based real estate app using Replicate API and OpenAI. We used Express, Typescript, OpenAI, Replicate, Stripe, and Mongoose to develop the API. We utilized NextJS, Formik, TailwindCSS, and other npm libraries for the UI. We have trained multiple AI assistants using the latest GPT model and integrated Replicate API for image processing. We added role-based auth, subscription plans, Cron job scheduling, and payment integration with Stripe.',
        tools: ['React', 'Bootstrap', 'SCSS', 'Stripe', 'Express'],
        code: '',
        role: 'Full Stack Developer',
        demo: '',
        image: realEstate,
    },
   
];


// Do not remove any property.
// Leave it blank instead as shown below

// {
//     id: 1,
//     name: '',
//     description: "",
//     tools: [],
//     role: '',
//     code: '',
//     demo: '',
//     image: crefin,
// },