export const experiences = [
  {
    id: 1,
    title: 'Development Team Lead',
    company: "Jpencil Pvt Ltd",
    duration: "(Dec 2022 - Present)"
  },
  {
    id: 2,
    title: "Web Developer",
    company: "Nutz Technovation Pvt Ltd",
    duration: "(Jun 2020 - Nov 2022)"
  },
  {
    id: 3,
    title: "Data Science Trainer - Part time",
    company: "ACTE",
    duration: "(Jan 2022 - Present)"
  }
]